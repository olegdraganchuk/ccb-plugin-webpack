/* eslint-disable no-undef */
jQuery(document).ready(function($) {
  var mediaUploader

  $('.color-field').wpColorPicker({
      defaultColor: '#66cccc',
      hide: true,
      change: function(event, ui) {},
      clear: function() {}
  })

  $('#add_custom_country_message').on('click', function() {
      var button = $(this)
      var counter = parseInt($(this).attr('data-custom-messages-count'))
      var textareaId = 'ccb_content_' + counter
      var data = {
        action: 'ccb_add_country_fields',
        textarea_id: textareaId
      }

      jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
        $(response).insertBefore(button)
        quicktags({id : textareaId})
        tinymce.execCommand('mceAddEditor', true, textareaId)
        $('#add_custom_country_message').attr('data-custom-messages-count', counter + 1)
      })
  })

  $(document).on('change', '.cbb_country_selector', function() {
    var forArea = $(this).attr('data-for')
    console.log('#' + forArea)
    $('#' + forArea).attr('name', 'ccb_content[' + $(this).val() + ']')
  })

  $(document).on('click', '.remove_custom_message', function() {
    $(this).parent('.ccb_content_wrapper').remove()
  })

  $('#upload_image_button').click(function(e) {
    e.preventDefault()
    if (mediaUploader) {
      mediaUploader.open()
      return
    }
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Choose Image',
      button: {
        text: 'Choose Image'
      },
      multiple: false
    })
    mediaUploader.on('select', function() {
      var attachment = mediaUploader.state().get('selection').first().toJSON()
      console.log(attachment)
      $('#ccb_icon').val(attachment.url)
      $('#ccb_icon_preview').attr('src', attachment.url)
    })
    mediaUploader.open()
  })
})
