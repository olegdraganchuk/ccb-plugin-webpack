/* eslint-disable no-undef */
jQuery(document).ready(function($) {
    var ccbGdpr = ccbReadCookie('ccbGdpr')
    if (ccbGdpr === null) {
        $('.ccb_cookie').addClass('show')
    }

    $('#ccb_cookie_agree').on('click', function(e) {
        e.preventDefault()
        $('.ccb_cookie').removeClass('show')
        ccbCreateCookie('ccbGdpr', 'agree', 9999)
    })

    $('#ccb_cookie_disagree').on('click', function(e) {
        e.preventDefault()
        $('.ccb_cookie').removeClass('show')
        ccbDeleteAllCookies()
        ccbCreateCookie('ccbGdpr', 'disagree', 9999)
    })

    function ccbCreateCookie(name, value, days) {
        var expires
        if (days) {
            var date = new Date()
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
            expires = '; expires=' + date.toGMTString()
        } else {
            expires = ''
        }
        document.cookie = name + '=' + value + expires + '; path=/'
    }

    function ccbReadCookie(name) {
        var nameEQ = name + '='
        var ca = document.cookie.split(';')
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i]
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c.length)
            }
            if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length, c.length)
            }
        }
        return null
    }

    function ccbDeleteAllCookies() {
        var cookies = document.cookie.split(';')
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i]
            var eqPos = cookie.indexOf('=')
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie
            document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT'
        }
    }
})
