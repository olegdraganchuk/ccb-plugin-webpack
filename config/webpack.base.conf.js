const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const PATHS = {
    src: path.join(__dirname, '../src'),
    dist: path.join(__dirname, '../../ccb-plugin'),
    public: '/public',
    admin: '/admin',
    languages: '/languages'
}

module.exports = {
    externals: {
        paths: PATHS
    },
    entry: {
        public: `${PATHS.src}${PATHS.public}`,
        admin: `${PATHS.src}${PATHS.admin}`
    },
    output: {
        filename: `[name]/js/script.js`,
        path: PATHS.dist,
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: '/node_modules/'
        },  {
            test: /\.(png|jpg|gif|svg)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        },  {
            test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        },  {
            test: /\.scss$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: { sourceMap: true }
                }, {
                    loader: 'postcss-loader',
                    options: { sourceMap: true }
                }, {
                    loader: 'sass-loader',
                    options: { sourceMap: true }
                }
            ]
        },  {
            test: /\.po$/,
            loader: 'file-loader?name=[name].mo!po2mo-loader'
        }
        ]
    },
    devServer: {
        overlay: true
    },
    resolve: {
        alias: {
            '~': 'src'
        }
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: `[name]/css/style.css`
        }),
        new HtmlWebpackPlugin({
            hash: false,
            template: `${PATHS.src}${PATHS.public}/banner.php`,
            filename: `${PATHS.public}/banner.php`
        }),
        new HtmlWebpackPlugin({
            hash: false,
            template: `${PATHS.src}${PATHS.admin}/index.html`,
            filename: `${PATHS.admin}/index.html`
        }),
        new CopyWebpackPlugin([
            { from: `./src${PATHS.public}/img`, to: `${PATHS.dist}${PATHS.public}/img` },
            { from: `./src${PATHS.admin}/img`, to: `${PATHS.dist}${PATHS.admin}/img` }
        ])
    ]
}
